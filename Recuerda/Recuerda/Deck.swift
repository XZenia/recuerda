//
//  Deck.swift
//  Recuerda
//
//  Created by Treskie on 06/10/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import Foundation
import Firebase

class Deck {
    var deckId: String
    var deckName: String
    
    init(deckId: String, deckName: String) {
        self.deckId = deckId
        self.deckName = deckName
    }
    
}
