//
//  AccountSignUpController.swift
//  Recuerda
//
//  Created by Treskie on 19/10/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import UIKit
import FirebaseAuth
class AccountSignUpController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var FullNameTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var ConfirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                self.segueToMainStoryboard()
            }
        }
        FullNameTextField.delegate = self as UITextFieldDelegate
        EmailTextField.delegate = self as UITextFieldDelegate
        PasswordTextField.delegate = self  as UITextFieldDelegate
        ConfirmPasswordTextField.delegate = self as UITextFieldDelegate
    
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func CreateButtonClicked(_ sender: UIButton) {
        var errorString = ""
        
        if (FullNameTextField.text?.isEmpty == true){
            errorString.append("Full name text field is empty!\n")
        }
        
        if (EmailTextField.text?.isEmpty == true){
            errorString.append("Email text field is empty!\n")
        } else if (validateEmail(email: EmailTextField.text!) == false){
            errorString.append("The email you entered is not a valid email!\n")
        }
        
        if (PasswordTextField.text?.isEmpty == true || ConfirmPasswordTextField.text?.isEmpty == true){
            errorString.append("Password fields cannot be empty!\n")
        } else if (PasswordTextField.text != ConfirmPasswordTextField.text){
            errorString.append("Passwords do not match!")
        }
        
        if (errorString == ""){
            Auth.auth().createUser(withEmail: EmailTextField.text!, password: PasswordTextField.text!) { user, error in
                if error == nil {
                    Auth.auth().currentUser?.createProfileChangeRequest().displayName = self.FullNameTextField.text
                    Auth.auth().signIn(withEmail: self.EmailTextField.text!,
                                       password: self.PasswordTextField.text!)
                }
            }
        } else {
            let alert = UIAlertController(title: "Account creation failed", message: errorString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func validateEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
    }
    
    func segueToMainStoryboard(){
        performSegue(withIdentifier: "AccountSignUp_MainStoryboardSegue", sender: self)
    }
}
