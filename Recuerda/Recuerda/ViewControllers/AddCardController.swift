//
//  AddCardController.swift
//  Recuerda
//
//  Created by Treskie on 13/10/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class AddCardController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate {

    @IBOutlet weak var QuestionTextView: UITextView!
    @IBOutlet weak var AnswerTextView: UITextView!
    @IBOutlet weak var NotesTextView: UITextView!
    
    var deckId: String!
    var databaseReference: DatabaseReference!
    var progress: Double! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        databaseReference = Database.database().reference(withPath: "Card")
        
        QuestionTextView.delegate = self as UITextViewDelegate
        AnswerTextView.delegate = self as UITextViewDelegate
        NotesTextView.delegate = self as UITextViewDelegate
        
        QuestionTextView.delegate = self
        AnswerTextView.delegate = self
        NotesTextView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)) , name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardWillChange(notification: Notification){

        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        let screenY = UIScreen.main.bounds.height
        let shrunkViewHeight = screenY - keyboardRect.height
        let screenShift = shrunkViewHeight / 2 - screenY / 2
        
        if NotesTextView.isFirstResponder {
            view.frame.origin.y = -keyboardRect.height
        } else {
            view.frame.origin.y = 0
        }
    }
    
    override var isFirstResponder: Bool {
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        view.frame.origin.y = 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //TODO: ADD ERROR POPUP WHEN USER DOES NOT ENTER ANY INPUT.
    @IBAction func AddFlashcardButtonClicked(_ sender: Any) {
        var errorString = ""
        
        if (QuestionTextView.text == ""){
            errorString.append("Question text field is empty!\n")
        }
        
        if (AnswerTextView.text == ""){
            errorString.append("Answer text field is empty!")
        }
        
        if (errorString == ""){
            if (NotesTextView.text == ""){
                NotesTextView.text = " "
            }
            saveCard(question: QuestionTextView.text!, answer: AnswerTextView.text!, notes: NotesTextView.text!)
        } else {
            let alert = UIAlertController(title: "Adding of card failed", message: errorString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }

    public func saveCard(question: String, answer: String, notes: String){
        let childData = databaseReference.child("CardList").childByAutoId()
        
        childData.setValue(["CardID": childData.key,
                            "DeckID": deckId,
                            "Question": question,
                            "Answer": answer,
                            "Notes": notes])
        segueToCardList()
    }
    
    @IBAction func BackButtonClicked(_ sender: UIButton) {
        segueToCardList()
    }
    
    func segueToCardList(){
        performSegue(withIdentifier: "AddCard_CardListSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddCard_CardListSegue" {
            let destinationViewController = segue.destination as! CardListController
            destinationViewController.deckId = deckId
        }
    }
}
