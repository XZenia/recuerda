//
//  CardImageController.swift
//  Recuerda
//
//  Created by Treskie on 20/10/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import Foundation
import UIKit

extension AddCardController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @objc func cardImageViewHandler(){
        let picker = UIImagePickerController()
        picker.delegate = self
        presentingViewController(picker,animation: true, completion: nil)
    }
}
