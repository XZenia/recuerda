//
//  CardListController.swift
//  Recuerda
//
//  Created by Treskie on 11/10/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
class CardListController: UIViewController, UITableViewDelegate, UITableViewDataSource  {

    var deckId: String!
    var databaseReference: DatabaseReference!
    var cardList: [Card] = []
    var selectedCard: Card!
    @IBOutlet weak var CardTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CardTableView.backgroundColor = UIColor.clear
        databaseReference = Database.database().reference(withPath: "Card")
        DispatchQueue.main.async { self.getAllCards() }
    }
    
    @IBAction func AddButtonClicked(_ sender: UIButton) {
        segueToAddCards()
    }
    
    @IBAction func ShareButtonClicked(_ sender: UIButton) {
        segueToQRCodeGenerator()
    }
    @IBAction func CardListClicked(_ sender: UIButton) {
        if (cardList.count > 0){
            segueToCardViewer()
        } else {
            let alert = UIAlertController(title: "No cards found", message: "Deck is empty! Please add cards in order to view them.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCard = cardList[indexPath.row]
        segueToEditCard()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "card", for: indexPath) as! CardTableViewCell
        let cardItem: Card
        cardItem = cardList[indexPath.row]
        cell.question.text = cardItem.question
        cell.answer.text = cardItem.answer
        return cell
    }
    
    public func getAllCards(){
        let cardReference = databaseReference.child("CardList").queryOrdered(byChild: "DeckID").queryEqual(toValue: deckId)
        cardReference.observe(DataEventType.value, with: { snapshot in
            if (snapshot.childrenCount > 0){
                self.cardList.removeAll()
                for child in snapshot.children.allObjects as! [DataSnapshot] {
                    let cardObject = child.value as? [String: AnyObject]
                    let cardId  = cardObject?["CardID"]
                    let deckId = cardObject?["DeckID"]
                    let question = cardObject?["Question"]
                    let answer = cardObject?["Answer"]
                    let notes = cardObject?["Notes"]
                    
                    let card = Card(deckId: (deckId as! String?)!, cardId: (cardId as! String?)!, question: (question as! String?)!, answer: (answer as! String?)!, notes: (notes as! String?)!)
                    self.cardList.append(card)
                }
            }
            self.CardTableView.reloadData()
        })
    }
    
    public func segueToAddCards(){
        performSegue(withIdentifier: "AddFlashcardSegue", sender: self)
    }
    
    public func segueToEditCard(){
        performSegue(withIdentifier: "EditFlashcardSegue", sender: self)
    }
    
    public func segueToCardViewer(){
        performSegue(withIdentifier: "CardViewerSegue", sender: self)
    }
    
    public func segueToQRCodeGenerator(){
        if (cardList.count > 0){
            performSegue(withIdentifier: "QRCodeGeneratorSegue", sender: self)
        } else {
            let alert = UIAlertController(title: "No cards found", message: "Deck is empty! Please add cards in order to use the QR Sharing feature.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddFlashcardSegue"{
            let destinationViewController = segue.destination as! AddCardController
            destinationViewController.deckId = deckId
        }
        
        if segue.identifier == "EditFlashcardSegue"{
            let destinationViewController = segue.destination as! EditCardController
            destinationViewController.card = selectedCard
        }
        
        if segue.identifier == "CardViewerSegue"{
            let destinationViewController = segue.destination as! CardViewerController
            destinationViewController.cardList = self.cardList
        }
        
        if segue.identifier == "QRCodeGeneratorSegue"{
            let destinationViewController = segue.destination as! QRCodeGeneratorController
            destinationViewController.deckId = deckId
        }
        
        
    }
}
