import UIKit

class CardTableViewCell: UITableViewCell {

    let question = makeSampleLabel()
    let answer = makeSampleLabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpLabelsAndConstraints()
    }
    
    private static func makeSampleLabel() -> UILabel {
        let label = UILabel()
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.numberOfLines = 0
        return label
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpLabelsAndConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpLabelsAndConstraints()
    }
    
    private func setUpLabelsAndConstraints(){
        question.font = UIFont.preferredFont(forTextStyle: .headline)
        question.adjustsFontForContentSizeCategory = true
        
        guard let palatino = UIFont(name: "Palatino", size: 18) else {
            fatalError("""
                Failed to load the "Palatino" font.
                Since this font is included with all versions of iOS that support Dynamic Type, verify that the spelling and casing is correct.
                """
            )
        }
        answer.font = UIFontMetrics(forTextStyle: .body).scaledFont(for: palatino)
        answer.adjustsFontForContentSizeCategory = true
        
        contentView.addSubview(answer)
        contentView.addSubview(question)
        
        question.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor).isActive = true
        question.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor).isActive = true
        
        answer.leadingAnchor.constraint(equalTo: question.leadingAnchor).isActive = true
        answer.trailingAnchor.constraint(equalTo: question.trailingAnchor).isActive = true
        
        question.firstBaselineAnchor.constraint(equalToSystemSpacingBelow: contentView.layoutMarginsGuide.topAnchor, multiplier: 1).isActive = true
        
        contentView.layoutMarginsGuide.bottomAnchor.constraint(equalToSystemSpacingBelow: answer.lastBaselineAnchor, multiplier: 1).isActive = true
        
        answer.firstBaselineAnchor.constraint(equalToSystemSpacingBelow: question.lastBaselineAnchor, multiplier: 1).isActive = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
