import UIKit

class CardViewerController: UIViewController {
    
    @IBOutlet weak var QuestionLabel: UILabel!
    @IBOutlet weak var AnswerLabel: UILabel!
    @IBOutlet weak var NotesLabel: UILabel!
    
    var cardList: [Card] = []
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillLabels()
    }
    
    @IBAction func FlipCardButtonClicked(_ sender: UIButton) {
        if (AnswerLabel.isHidden){
            AnswerLabel.isHidden = false
            NotesLabel.isHidden = false
            QuestionLabel.isHidden = true
            
        } else {
            AnswerLabel.isHidden = true
            NotesLabel.isHidden = true
            QuestionLabel.isHidden = false
        }
    }
    
    @IBAction func ShuffleButtonClicked(_ sender: UIButton) {
        if (cardList.count > 1){
            cardList.shuffle()
            counter = 0;
            fillLabels()
        } else {
            let alert = UIAlertController(title: "More cards needed", message: "You need more than 1 card to shuffle the deck. Please add more cards to your deck.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    func previousCard(){
        if (counter <= 0){
            counter = cardList.count - 1
        } else {
            counter -= 1
        }
        fillLabels()
    }
    
    func nextCard(){
        if (counter >= cardList.count - 1){
            counter = 0
        } else {
            counter += 1
        }
        fillLabels()
    }
    
    @IBAction func PreviousButtonClicked(_ sender: UIButton) {
        previousCard()
    }
    
    @IBAction func NextButton(_ sender: UIButton) {
        nextCard()
    }
    
    func fillLabels(){
        self.QuestionLabel.text = self.cardList[self.counter].question
        self.AnswerLabel.text = self.cardList[self.counter].answer
        self.NotesLabel.text = self.cardList[self.counter].notes
        
        NotesLabel.isHidden = true
        AnswerLabel.isHidden = true
        QuestionLabel.isHidden = false
    }
    
    @IBAction func BackButtonClicked(_ sender: UIButton) {
        segueToCardList()
    }
    
    func segueToCardList(){
        performSegue(withIdentifier: "CardViewer_CardListSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CardViewer_CardListSegue" {
            let destinationViewController = segue.destination as! CardListController
            destinationViewController.deckId = cardList[counter].deckId
        }
    }
}
