import UIKit
import Firebase

class DeckListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var databaseReference: DatabaseReference!
    var deckList: [Deck] = []
    var selectedDeck: Deck!
    var deckNameTextField: UITextField?
    let userID: String! = Auth.auth().currentUser!.uid
    @IBOutlet weak var DeckTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DeckTableView.backgroundColor = UIColor.clear //sets transparent background of table view
        databaseReference = Database.database().reference(withPath: "Deck")
        self.getAllDecks()
    }
    
    @IBAction func SignOutButtonClicked(_ sender: UIButton) {
        do
        {
            try Auth.auth().signOut()
            performSegue(withIdentifier: "AccountStoryboardSegue", sender: self)
        }
        catch let error as NSError
        {
            let alert = UIAlertController(title: "Sign Out Failed", message: "\(error)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deckList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deck", for: indexPath) as! DeckTableViewCell
        let deckItem: Deck
        deckItem = deckList[indexPath.row]
        cell.labelDeck.text = deckItem.deckName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        segueToCardList(deck: deckList[indexPath.row])
    }
    
    @IBAction func displayAddDeckDialog(_ sender: UIButton) {
        //Creating the Alert Dialog
        let alertController = UIAlertController(title: "Add a new deck", message: nil, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: deckNameTextField)
        //Adding buttons to alert dialog
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {
            [unowned self] (action) -> Void in self.saveNewDeck() })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        //Showing the alert dialog
        self.present(alertController, animated: true)
    }
    
    func segueToCardList(deck: Deck){
        self.selectedDeck = deck
        performSegue(withIdentifier: "SegueToCardList", sender: self)
    }
    
    func saveNewDeck(){
        if (deckNameTextField?.text != ""){
            self.saveDeck(deckName: (deckNameTextField?.text)!)
        }
    }
    
    //New Deck Alert Dialog placeholder
    func deckNameTextField(textField: UITextField!){
        deckNameTextField = textField
        deckNameTextField?.placeholder = "Name of Deck"
    }
    
    public func getAllDecks(){
        let deckReference = databaseReference.child("DeckList").queryOrdered(byChild: "UserID").queryEqual(toValue: userID)
        deckReference.observe(DataEventType.value, with: { snapshot in
            if (snapshot.childrenCount > 0){
                self.deckList.removeAll()
                for child in snapshot.children.allObjects as! [DataSnapshot] {
                    let deckObject = child.value as? [String: AnyObject]
                    let deckName  = deckObject?["DeckName"]
                    let deckId  = deckObject?["DeckID"]
                    let deck = Deck(deckId: (deckId as! String?)!, deckName: (deckName as! String?)!)
                    
                    self.deckList.append(deck)
                }
            }
            self.DeckTableView.reloadData()
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueToCardList"{
            let destinationViewController = segue.destination as! CardListController
            destinationViewController.deckId = selectedDeck.deckId
        }
    }
    
    public func saveDeck(deckName: String){
        let childData = databaseReference.child("DeckList").childByAutoId()
        childData.setValue(["DeckID": childData.key,
                            "DeckName": deckName,
                            "UserID": userID])
        let deck: Deck! = Deck(deckId: childData.key!, deckName: deckName)
        selectedDeck = deck
        self.DeckTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let groupRef = databaseReference.child("DeckList").child(deckList[indexPath.row].deckId)
            let cardRef = Database.database().reference().child("Card").child("CardList")
            
            let cardQuery = cardRef.queryOrdered(byChild: "DeckID").queryEqual(toValue: deckList[indexPath.row].deckId)
            cardQuery.observe(DataEventType.value, with: { snapshot in
                if (snapshot.childrenCount > 0){
                    for child in snapshot.children.allObjects as! [DataSnapshot] {
                        let cardObject = child.value as? [String: AnyObject]
                        let deleteRef = cardRef.child(cardObject?["CardID"] as! String)
                        deleteRef.removeValue()
                    }
                }
            })
            groupRef.removeValue()
            deckList.remove(at: indexPath.row)
            DeckTableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
