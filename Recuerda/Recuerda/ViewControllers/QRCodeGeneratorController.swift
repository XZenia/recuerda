//
//  QRCodeGeneratorController.swift
//  Recuerda
//
//  Created by Treskie on 01/11/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import UIKit

class QRCodeGeneratorController: UIViewController {
    
    @IBOutlet weak var QRCodeImageView: UIImageView!
    
    var deckId: String!
    lazy var filter = CIFilter(name: "CIQRCodeGenerator")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateCode(deckId)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func BackButtonClicked(_ sender: UIButton) {
        segueToCardList()
    }
    
    func generateCode(_ string: String, foregroundColor: UIColor = .black, backgroundColor: UIColor = .white) {
        guard let filter = filter,
            let data = string.data(using: .isoLatin1, allowLossyConversion: false) else {
                return
        }
        
        filter.setValue(data, forKey: "inputMessage")
        
        guard let ciImage = filter.outputImage else {
            return
        }
        
        let transformed = ciImage.transformed(by: CGAffineTransform.init(scaleX: 10, y: 10))
        let invertFilter = CIFilter(name: "CIColorInvert")
        invertFilter?.setValue(transformed, forKey: kCIInputImageKey)
        
        let alphaFilter = CIFilter(name: "CIMaskToAlpha")
        alphaFilter?.setValue(invertFilter?.outputImage, forKey: kCIInputImageKey)
        
        if let outputImage = alphaFilter?.outputImage  {
            QRCodeImageView.tintColor = foregroundColor
            QRCodeImageView.backgroundColor = backgroundColor
            QRCodeImageView.image = UIImage(ciImage: outputImage, scale: 2.0, orientation: .up)
                .withRenderingMode(.alwaysTemplate)
        }
    }
    
    func segueToCardList(){
        performSegue(withIdentifier: "QRCodeGenerator_CardListSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "QRCodeGenerator_CardListSegue"{
            let destinationViewController = segue.destination as! CardListController
            destinationViewController.deckId = deckId
        }
    }
}
