//
//  QRCodeScannerController.swift
//  Recuerda
//
//  Created by Treskie on 03/11/2018.
//  Copyright © 2018 Treskie. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import Firebase
import FirebaseAuth

class QRCodeScannerController: UIViewController, QRCodeReaderViewControllerDelegate {

    var cardList: [Card] = []
    var databaseReference: DatabaseReference!

    @IBOutlet weak var ErrorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        databaseReference = Database.database().reference()
    }
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    @IBAction func scanAction(_ sender: AnyObject) {
        if (QRCodeReader.isAvailable()){
            readerVC.delegate = self
            
            readerVC.completionBlock = { (result: QRCodeReaderResult!) in
                self.cloneDeck(deckId: result.value)
                print(result.value)
            }
            
            readerVC.modalPresentationStyle = .formSheet
            present(readerVC, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Cannot start QR scanner", message: "Your device does not support the QR Code reader library. Please try again on a compatible device.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true)
        }
    }

    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    func cloneDeck(deckId: String){
        var deckName: String = ""
        let deckReference = databaseReference.child("Deck").child("DeckList").queryOrdered(byChild: "DeckID").queryEqual(toValue: deckId)

        deckReference.observe(DataEventType.value, with: { snapshot in
            if (snapshot.childrenCount > 0){
                for child in snapshot.children.allObjects as! [DataSnapshot] {
                    let deckObject = child.value as? [String: AnyObject]
                    let name = deckObject?["DeckName"]
                    deckName = name as! String
                    
                    self.getCards(deckId: deckId, deckName: deckName)
                    break
                }
            } else {
                let alert = UIAlertController(title: "Deck does not exist", message: "Please scan a QR code of a valid deck and try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        })

    }
    
    func getCards(deckId: String, deckName: String){
        let cardReference = databaseReference.child("Card").child("CardList").queryOrdered(byChild: "DeckID").queryEqual(toValue: deckId)
        
        cardReference.observe(DataEventType.value, with: { snapshot in
            if (snapshot.childrenCount > 0){
                for child in snapshot.children.allObjects as! [DataSnapshot] {
                    let cardObject = child.value as? [String: AnyObject]
                    let question = cardObject?["Question"]
                    let answer = cardObject?["Answer"]
                    let notes = cardObject?["Notes"]
                    
                    let card = Card(deckId: "", cardId: "", question: (question as! String?)!, answer: (answer as! String?)!, notes: (notes as! String?)!)
                    self.cardList.append(card)
                }
                
                self.saveData(deckName: deckName)
                self.segueToDeckList()
            } else {
                let alert = UIAlertController(title: "Cards missing from deck", message: "Cards are missing from the shared deck. Please scan a deck with cards in them.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            }
        })
        
    }
    
    func saveData(deckName: String){
        let childData = databaseReference.child("Deck").child("DeckList").childByAutoId()
        childData.setValue(["DeckID": childData.key,
                            "DeckName": deckName,
                            "UserID": Auth.auth().currentUser!.uid])
        
        if (cardList.count > 0){
            for card in cardList{
                saveCard(question: card.question, answer: card.answer, notes: card.notes, newDeckId: childData.key!)
            }
        }
    }
    
    public func saveCard(question: String, answer: String, notes: String, newDeckId: String){
        let childData = databaseReference.child("Card").child("CardList").childByAutoId()
        
        childData.setValue(["CardID": childData.key,
                            "DeckID": newDeckId,
                            "Question": question,
                            "Answer": answer,
                            "Notes": notes])
    }
    
    @IBAction func BackButtonClicked(_ sender: UIButton) {
        segueToDeckList()
    }
    
    func segueToDeckList(){
        performSegue(withIdentifier: "QRCodeScanner_DeckListSegue", sender: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
